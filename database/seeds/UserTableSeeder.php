<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;

class UserTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->insert(
            [
                [
                    'name' => 'yoram',
                    'email' => 'a@a.com ',
                    'password' =>'12345678',
                    'created_at' => date('Y-m-d G:i:s'),
                    'role' => 'admin',
                ],
                
            ]);
        }
    
}
